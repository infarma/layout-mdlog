package layout_mdlog

import (
	"bitbucket.org/infarma/layout-mdlog/arquivoDePedido"
	"os"
)

func GetArquivoDePedido(fileHandle *os.File) (arquivoDePedido.ArquivoDePedido, error) {
	return arquivoDePedido.GetStruct(fileHandle)
}
