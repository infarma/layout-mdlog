## Arquivo de Pedido
gerador-layouts arquivoDePedido cabecalho CodigoRegistro:string:0:2 IdentificacaoPedidoCliente:string:2:14 CodigoCliente:string:14:24 CnpjCliente:int64:24:38 CnpjFornecedor:int64:38:52 DataPedido:int32:52:60

gerador-layouts arquivoDePedido detalhe CodigoRegistro:string:0:2 IdentificacaoPedidoProduto:int32:2:3 IdentificacaoProduto:string:3:17 CondicaoComercial:string:17:23 EmbalagemProduto:string:23:25 QuantidadePedida:float64:25:36:3 PrecoProduto:float64:36:52:5 DescontoItem:float32:52:56:2 DescontoRepasseICMS:float32:56:60:2 DescontoComercial:float32:60:64:2 CodigoProjeto:string:64:71 DescontoProjeto:float32:71:75:2

gerador-layouts arquivoDePedido rodape CodigoRegistro:string:0:2 TotalItens:int32:2:8 TotalQuantidadePedida:float64:8:19:3 ValorTotalBrutoPedido:float64:19:35:5



## Arquivo de Retorno
gerador-layouts retornoDePedido cabecalho CodigoRegistro:string:0:2 IdentificacaoPedidoCliente:string:2:14 CnpjFornecedor:int64:14:28 IdentificacaoInicialPedidoFornecedor:string:28:40 IdentificacaoFinalPedidoFornecedor:string:40:52 PrevisaoDataFaturamento:int32:52:60 PrevisaoDataEntregaMercadoria:int32:60:68 StatusProcessamentoPedido:int32:68:69

gerador-layouts retornoDePedido detalhe CodigoRegistro:string:0:2 TipoIdentificacaoProduto:int32:2:3 IdentificacaoProduto:string:3:17 QuantidadeAtendida:float64:17:28:3 QuantidadeRecusada:float64:28:39:3 MotivoRejeicao:string:39:69

gerador-layouts retornoDePedido rodape CodigoRegistro:string:0:2 TotalItensAtendidos:int32:2:8 TotalQuantidadeAtendida:float64:8:19:3 TotalItensRecusados:int32:19:25 TotalQuantidadeRecusado:float64:25:36:3


## Arquivo de Nota Fiscal
gerador-layouts arquivoDeNotaFiscal cabecalho CodigoRegistro:string:0:2 IdentificacaoPedidoCliente:string:2:14 CnpjEmissorNotaFiscal:string:14:28 NumeroNotaFiscal:int32:28:34 SerieNotaFiscal:string:34:37 DataEmissaoNotaFiscal:int32:37:45 CodigoFiscalOperacao:int32:45:49 CodigoValorFiscal:int32:49:50 ValorBrutoNotaFiscal:float64:50:65:2 ValorContabilNotaFiscal:float64:65:80:2 ValorTributadoNotaFiscal:float64:80:95:2 ValorOutrasNotaFiscal:float64:95:110:2 BaseICMSRetido:float64:110:125:2 ValorICMSRetido:float64:125:140:2 BaseICMSEntrada:float64:140:155:2 ValorICMSEntrada:float64:155:170:2 BaseIPI:float64:170:185:2 ValorIPI:float64:185:200:2 CGCTransportadora:string:200:214 TipoFrete:string:214:215 ValorFrete:float64:215:230:2 PercentualDescontoComercial:float32:230:236:4 PercentualDescontoRepasseICMS:float32:236:242:4 TotalItens:int64:242:255 TotalUnidades:float64:255:266:3 TotalNotasParaPedido:int32:266:272 ValorDescontoICMSNormal:float64:272:287:2 ValorDescontoPIS:float64:287:302:2 ValorDescontoCofins:float64:302:317:2 TipoNotaFiscalEntrada:int32:317:319 ChaveAcessoNotaFiscalEletronica:string:319:363

gerador-layouts arquivoDeNotaFiscal produtos CodigoRegistro:string:0:2 TipoIdentificacaoProduto:int32:2:3 IdentificacaoProduto:string:3:17 EmbalagemProduto:string:17:19 QuantidadeEmbalagens:float64:19:30:3 CodigoFiscalOperacao:int32:30:34 SubstituicaoTributaria:string:34:36 PrecoProduto:float64:36:51:5 PercentualDescontoItem:float32:51:57:4 ValorDescontoItem:float64:57:72:5 PercentualDescontoRepasse:float32:72:78:4 ValorDescontoRepasse:float64:78:93:5 PercentualDescontoComercial:float32:93:99:4 ValorDescontoComercial:float64:99:114:5 ValorDespesaAcessorias:float64:114:129:5 ValorDespesaEmbalagem:float64:129:144:5

gerador-layouts arquivoDeNotaFiscal lote CodigoRegistro:string:0:2 LoteFabricacao:string:2:22 QuantidadeProduto:float64:22:33:3 DataValdidade:int32:33:41 DataFabricacao:int32:41:49

gerador-layouts arquivoDeNotaFiscal titulos CodigoRegistro:string:0:2 NumeroTitulo:string:2:12 LocalPagamento:string:12:16 DataVencimento:int32:16:24 ValorBruto:float64:24:39:2 ValorLiquido:float64:39:54:2 DataAntecipacao:int32:54:62 DescontoAntecipado:float32:62:66:2 DescontoLimite:float32:66:70:2