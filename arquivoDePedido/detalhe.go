package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Detalhe struct {
	CodigoRegistro             string  `json:"CodigoRegistro"`
	IdentificacaoPedidoProduto int32   `json:"IdentificacaoPedidoProduto"`
	IdentificacaoProduto       string  `json:"IdentificacaoProduto"`
	CondicaoComercial          string  `json:"CondicaoComercial"`
	EmbalagemProduto           string  `json:"EmbalagemProduto"`
	QuantidadePedida           float64 `json:"QuantidadePedida"`
	PrecoProduto               float64 `json:"PrecoProduto"`
	DescontoItem               float32 `json:"DescontoItem"`
	DescontoRepasseICMS        float32 `json:"DescontoRepasseICMS"`
	DescontoComercial          float32 `json:"DescontoComercial"`
	CodigoProjeto              string  `json:"CodigoProjeto"`
	DescontoProjeto            float32 `json:"DescontoProjeto"`
}

func (d *Detalhe) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDetalhe

	err = posicaoParaValor.ReturnByType(&d.CodigoRegistro, "CodigoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.IdentificacaoPedidoProduto, "IdentificacaoPedidoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.IdentificacaoProduto, "IdentificacaoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CondicaoComercial, "CondicaoComercial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.EmbalagemProduto, "EmbalagemProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.QuantidadePedida, "QuantidadePedida")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PrecoProduto, "PrecoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.DescontoItem, "DescontoItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.DescontoRepasseICMS, "DescontoRepasseICMS")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.DescontoComercial, "DescontoComercial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CodigoProjeto, "CodigoProjeto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.DescontoProjeto, "DescontoProjeto")
	if err != nil {
		return err
	}

	return err
}

var PosicoesDetalhe = map[string]gerador_layouts_posicoes.Posicao{
	"CodigoRegistro":             {0, 2, 0},
	"IdentificacaoPedidoProduto": {2, 3, 0},
	"IdentificacaoProduto":       {3, 17, 0},
	"CondicaoComercial":          {17, 23, 0},
	"EmbalagemProduto":           {23, 25, 0},
	"QuantidadePedida":           {25, 36, 3},
	"PrecoProduto":               {36, 52, 5},
	"DescontoItem":               {52, 56, 2},
	"DescontoRepasseICMS":        {56, 60, 2},
	"DescontoComercial":          {60, 64, 2},
	"CodigoProjeto":              {64, 71, 0},
	"DescontoProjeto":            {71, 75, 2},
}
