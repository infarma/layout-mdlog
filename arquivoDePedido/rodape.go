package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Rodape struct {
	CodigoRegistro        string  `json:"CodigoRegistro"`
	TotalItens            int32   `json:"TotalItens"`
	TotalQuantidadePedida float64 `json:"TotalQuantidadePedida"`
	ValorTotalBrutoPedido float64 `json:"ValorTotalBrutoPedido"`
}

func (r *Rodape) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRodape

	err = posicaoParaValor.ReturnByType(&r.CodigoRegistro, "CodigoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.TotalItens, "TotalItens")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.TotalQuantidadePedida, "TotalQuantidadePedida")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ValorTotalBrutoPedido, "ValorTotalBrutoPedido")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRodape = map[string]gerador_layouts_posicoes.Posicao{
	"CodigoRegistro":        {0, 2, 0},
	"TotalItens":            {2, 8, 0},
	"TotalQuantidadePedida": {8, 19, 3},
	"ValorTotalBrutoPedido": {19, 35, 5},
}
